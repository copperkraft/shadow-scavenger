# Shadow Scavenger

This is a project to put together all my experiments


## Get started

- Clone this repo
- Install modules
- Change DB connection string in `./config/default.json`

```
npm i
```

- Building commands are included in `postinstall` script, so you can start project

```
npm start
```

## Development

- to build client in watch mode run 
```
npm run build.client
```
- to build server in watch mode run
```
npm run build.server
```
note, that after changes you need to restart the server

- to lint project run
```
npm run lint.server
npm run lint.client
```
