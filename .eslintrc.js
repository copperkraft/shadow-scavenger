module.exports = {
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: "module",
        project: "./tsconfig.json",
    },
    extends: [
        "airbnb-typescript/base"
    ],
    rules: {
        "import/prefer-default-export": 0,
        "no-param-reassign": [
            "error",
            {
                "props": true,
                "ignorePropertyModificationsForRegex": [
                    "connection",
                    "req",
                    "res"
                ]
            }],
        "max-len": ["error", { "code": 120 }],
        "linebreak-style": 0,
        "class-methods-use-this": 0,
    }
};
