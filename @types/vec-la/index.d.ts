// tslint:disable:max-line-length

type Vector = [number, number];

declare module 'vec-la' {
  function add(v1: Vector, v2: Vector): Vector; // Result of adding v1 and v2
  function sub(v1: Vector, v2: Vector): Vector; // Result of subtracting v2 from v1
  function scale(v: Vector, scale: number): Vector; // Result of multiplying components of v by scale
  function midpoint(v1: Vector, v2: Vector): Vector; // Midpoint between v1 and v2
  function norm(v: Vector): Vector; // Result of normalising v
  function mag(v: Vector): number; // Magnitude of v
  function normal(v: Vector): Vector; // Normal vector of v
  function towards(v1: Vector, v2: Vector, percentage: number): Vector; // A point in the interval [v, v2] in percentage where the point falls
  function rotate(v: Vector, angle: number): Vector; // Result of rotating v around the origin by a radians
  function rotatePointAround(v: Vector, origin: Vector, angle: number): Vector; // Result of rotating v around origin by a radians
  function dist(v1: Vector, v2: Vector): number; // Euclidean distance between v1 and v2

  /*
    vec.dot(v, v2) : Dot product of v and v2
    vec.det(v) : Determinant of v
    vec.matrixBuilder(m) : Creates a matrix builder (see below)
    vec.createMatrix(a, b, c, d, tx, ty) : Helper function for matrix creation. Defaults to an identity matrix
    vec.transform(v, m) : Result of applying matrix transformation m to v
    vec.composeTransform(m, m2) : Result of composing transformation matrix m with m2
  */
}
