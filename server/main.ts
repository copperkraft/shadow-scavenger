import { iocContainer } from './utils/ioc-container';
import { runServers } from './net/index.server';
import { resolveJobs } from './jobs/resolver';
import { mongoInit } from './data/init';

runServers(iocContainer);
resolveJobs(iocContainer);
mongoInit();
