export default class User {
  id?: string;

  name: string;

  username: string;

  constructor(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.username = data.username;
  }
}
