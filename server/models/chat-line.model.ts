import { prop } from '@typegoose/typegoose';

export default class ChatLineModel {
  @prop()
  date?: Date;

  @prop()
  sender?: string;

  @prop()
  message?: string;
}
