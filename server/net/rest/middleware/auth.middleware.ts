import express from 'express';
import { Container } from 'inversify';
import AuthService from '../../../services/auth.service';
import { iocContainer } from '../../../utils/ioc-container';
import { verifyJWT } from '../../../utils/jwt';

function authMiddlewareFactory(container: Container) {
  return () => (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const authService = container.get<AuthService>(AuthService);

    (async () => {
      const token = req.get('x-auth-token');

      if (!token) {
        res.status(401).end('Unauthorized');
        return;
      }

      const { userId } = verifyJWT(token);

      if (typeof userId !== 'number') {
        res.status(401).end('Unauthorized');
      }

      const user = await authService.getUserById(userId);

      if (!user) {
        res.status(401).end('Unauthorized');
        return;
      }

      res.locals.user = user;

      next();
    })();
  };
}

const authMiddleware = authMiddlewareFactory(iocContainer);

export { authMiddleware };
