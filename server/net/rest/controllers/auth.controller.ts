import { Request, Response } from 'express';
import { inject } from 'inversify';
import {
  BaseHttpController, controller, httpGet, httpPost,
} from 'inversify-express-utils';
import { iocContainer } from '../../../utils/ioc-container';
import { authMiddleware } from '../middleware/auth.middleware';
import { signJWT } from '../../../utils/jwt';
import User from '../../../models/user.model';
import AuthService from '../../../services/auth.service';

@controller('/auth')
export class AuthController extends BaseHttpController {
  constructor(@inject(AuthService) private authService: AuthService) {
    super();
  }

  getUserResponse(user: User): any {
    return {
      user,
      token: signJWT({ id: user.id }),
    };
  }

  @httpPost('/register')
  async register(req: Request): Promise<any> {
    const userPartial = await this.authService.register(req.body);
    if (userPartial) {
      return this.json(this.getUserResponse(userPartial));
    }
    return this.json({ error: 'invalid credentials' }, 401);
  }

  @httpPost('/login')
  async login(req: Request): Promise<any> {
    const userPartial = await this.authService.authenticate(req.body);
    if (userPartial) {
      return this.json(this.getUserResponse(userPartial));
    }
    return this.json({ error: 'invalid credentials' }, 401);
  }

  @httpGet('/', authMiddleware())
  async getCurrent(req: Request, res: Response): Promise<any> {
    const { user } = res.locals;
    return this.json(user);
  }
}

iocContainer.bind<AuthController>(AuthController).toSelf();
