import {
  BaseHttpController, controller, httpGet, httpPost, request, response,
} from 'inversify-express-utils';
import { inject } from 'inversify';
import { Request, Response } from 'express';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { iocContainer } from '../../../utils/ioc-container';
import { ChatService } from '../../../services/chat.service';
import ChatLine from '../../../../shared/models/chat-line';

@controller('/chats')
export class ChatController extends BaseHttpController {
  constructor(@inject(ChatService) private chatService: ChatService) {
    super();
  }

  @httpPost('/channel')
  async post(): Promise<any> {
    this.chatService.createChannel(`channel${1000 + Math.floor(Math.random() * 9000)}`);
    return this.ok();
  }

  @httpGet('/channel/:id')
  async getLog(@request() req: Request, @response() res: Response): Promise<any> {
    const log: Observable<ChatLine[]> | null = this.chatService.getLog(req.params.id);

    if (!log) {
      return this.notFound();
    }

    log.pipe(first()).subscribe((chatLog) => {
      res.json(chatLog);
    });

    return this.json(await log.toPromise());
  }

  @httpGet('/channels')
  async getChannels(): Promise<any> {
    return this.json(this.chatService.channelNames);
  }
}

iocContainer.bind<ChatController>(ChatController).toSelf();
