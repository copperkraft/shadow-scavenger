import {
  Application, NextFunction, Request, Response,
} from 'express';

const errorResponses: { [key: string]: { message: string, code: number, shouldLog?: boolean } } = {
  'channel already exists': {
    message: 'Channel already exists',
    code: 409,
  },
  'Unable to sign in': {
    message: 'Unable to sign in',
    code: 403,
  },
  defaultResponse: {
    message: 'Something went wrong.',
    code: 500,
    shouldLog: true,
  },
};

export const restErrorHandler = (app: Application) => {
  // express checks if handler function has 4 arguments
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    const errorResponse = errorResponses[err.message] || errorResponses.defaultResponse;

    if (errorResponse.shouldLog) {
      // eslint-disable-next-line no-console
      console.log(req, res);
    }

    const { message, code } = errorResponse;
    res.status(code).send(message);
  });
};
