import { Container } from 'inversify';
import { InversifyExpressServer } from 'inversify-express-utils';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import { Server as WsServer } from 'ws';
import config from 'config';
import { staticServer } from './static.server';
import { wsServer } from './ws/ws.server';
import './rest/controllers';
import { restErrorHandler } from './rest/rest-error-handler';

export const runServers = (container: Container) => {
  const inversifyServer = new InversifyExpressServer(container, null, { rootPath: '/api' });

  inversifyServer.setConfig((app) => {
    app.use(bodyParser.urlencoded({
      extended: true,
    }));
    app.use(bodyParser.json());
  });

  inversifyServer.setErrorConfig(restErrorHandler);

  const app = inversifyServer.build();
  const server = http.createServer(app);

  container.bind<WsServer>(WsServer).toConstantValue(wsServer(server));

  staticServer(app);
  const port = config.get('port');

  server.listen(port);
  // eslint-disable-next-line no-console
  console.log(`server started, port:${port}`);
};
