import { Server as WsServer } from 'ws';
import { Server } from 'http';

export const wsServer = (server: Server) => new WsServer({ server });
