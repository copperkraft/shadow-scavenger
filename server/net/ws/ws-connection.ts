import { Message } from '../../../shared/models/message';
import User from '../../models/user.model';

export default class WsConnection {
  public user: User | null = null;

  constructor(private ws: WebSocket) { }

  send<T extends Message = Message>(message: T) {
    this.ws.send(JSON.stringify(message));
  }
}
