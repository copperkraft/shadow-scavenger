import { Server as WsServer } from 'ws';
import { Subject } from 'rxjs/internal/Subject';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { inject, injectable } from 'inversify';
import { iocContainer } from '../../utils/ioc-container';
import WsConnection from './ws-connection';
import { Message } from '../../../shared/models/message';

export interface WsMessage<T extends Message = Message> {
  message: T;
  connection: WsConnection;
}

@injectable()
export default class WsService {
  private messageSubject$ = new Subject<WsMessage>();

  private connectionSubject$ = new Subject<WsConnection>();

  constructor(@inject(WsServer) private wss: WsServer) {
    this.wss.on('connection', (ws: WebSocket) => {
      const connection = new WsConnection(ws);
      this.connectionSubject$.next(connection);

      ws.addEventListener('message', (event: MessageEvent) => {
        const message = JSON.parse(event.data);
        this.messageSubject$.next({ message, connection });
      });
    });
  }

  get $connection() {
    return this.connectionSubject$.asObservable();
  }

  on<T extends Message>(type: T['type']): Observable<WsMessage<T>> {
    return this.messageSubject$.pipe(
      filter((wsMessage) => wsMessage.message.type === type),
    ) as Observable<WsMessage<T>>;
  }

  broadCast<T extends Message = Message>(message: T) {
    this.wss.clients.forEach((ws) => ws.send(JSON.stringify(message)));
  }
}

iocContainer.bind<WsService>(WsService).toSelf().inSingletonScope();
