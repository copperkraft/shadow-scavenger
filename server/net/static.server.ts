import * as path from 'path';
import express from 'express';

export const staticServer = (app: express.Application) => {
  app.use(express.static(path.resolve(__dirname, '../../client')));
  app.use('/*', express.static(path.join(__dirname, '../../client/index.html')));
};
