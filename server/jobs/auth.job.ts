import { inject, injectable } from 'inversify';
import WsService from '../net/ws/ws.service';
import AuthService from '../services/auth.service';
import { iocContainer } from '../utils/ioc-container';
import { verifyJWT } from '../utils/jwt';
import { Message } from '../../shared/models/message';

export type AuthMessage = Message<'auth', string | null>;

@injectable()
export class AuthJob {
  constructor(
    @inject(WsService) public wsService: WsService,
    @inject(AuthService) public authService: AuthService,
  ) {
    wsService.on<AuthMessage>('auth')
      .subscribe(async ({ message, connection }) => {
        connection.user = null;

        const { payload: token } = message;

        if (!token) {
          return;
        }

        const { id } = verifyJWT(token);

        if (typeof id !== 'string') {
          return;
        }

        connection.user = await authService.getUserById(id);
      });
  }
}

iocContainer.bind<AuthJob>(AuthJob).toSelf().inSingletonScope();
