import { inject, injectable } from 'inversify';
import config from 'config';
import { first } from 'rxjs/internal/operators/first';
import { delay } from 'rxjs/operators';
import WsService from '../net/ws/ws.service';
import { iocContainer } from '../utils/ioc-container';
import ChatLine from '../../shared/models/chat-line';
import { Message } from '../../shared/models/message';
import { ChatService } from '../services/chat.service';

export type ConnectToChatMessage = Message<'connect to channel', string>;

export type ChatMessage = Message<'message', {
  channel: string;
  line: ChatLine
}>;

export type LogMessage = Message<'log', {
  channel: string,
  log: ChatLine[]
}>;

export type NewConnectionMessage = Message<'new connection', {
  channel: string,
  name: null | string
}>;

@injectable()
export class ChatJob {
  constructor(
    @inject(WsService) public wsService: WsService,
    @inject(ChatService) public chatService: ChatService,
  ) {
    wsService.on<ChatMessage>('message')
      .subscribe(({ message: { payload }, connection }) => {
        const { line, channel } = payload;
        const username = connection.user && connection.user.name;

        const authorizedLine: ChatLine = {
          ...line,
          sender: username || config.get('chat.unauthorizedName'),
          date: new Date(),
        };

        this.chatService.pushMessage(channel, authorizedLine);

        wsService.broadCast<ChatMessage>({
          type: 'message',
          payload: {
            channel,
            line: authorizedLine,
          },
        });
      });

    wsService.on<ConnectToChatMessage>('connect to channel')
      .pipe(delay(100))
      .subscribe(({ message, connection }) => {
        const log = this.chatService.getLog(message.payload);

        if (!log) {
          return;
        }

        log.pipe(first()).subscribe((logMessage: ChatLine[]) => connection.send<LogMessage>({
          type: 'log',
          payload: {
            log: logMessage,
            channel: message.payload,
          },
        }));

        const username = connection.user && connection.user.name;

        wsService.broadCast<NewConnectionMessage>({
          type: 'new connection',
          payload: {
            channel: message.payload,
            name: username || null,
          },
        });
      });
  }
}

iocContainer.bind<ChatJob>(ChatJob).toSelf().inSingletonScope();
