import { Container } from 'inversify';
import { ChatJob } from './chat.job';
import { AuthJob } from './auth.job';

const resolveJobs = (container: Container) => {
  container.resolve(ChatJob);
  container.resolve(AuthJob);
};

export { resolveJobs };
