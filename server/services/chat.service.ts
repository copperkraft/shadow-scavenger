import { inject, injectable } from 'inversify';
import { Observable } from 'rxjs';
import ChannelStore from '../../shared/store/chat.store';
import { iocContainer } from '../utils/ioc-container';
import ChatLine from '../../shared/models/chat-line';
import { ChannelRepository } from '../data/repositories/channel.repository';
import ChatChannel from '../../shared/models/chat-channel';

@injectable()
export class ChatService {
  private channelStores: ChannelStore[] = [];

  constructor(
    @inject(ChannelRepository) private channelRepository: ChannelRepository,
  ) {
    this.init();
  }

  public async init() {
    const channels = await this.channelRepository.getChannels();

    this.channelStores = channels.length
      ? channels.map((channel) => new ChannelStore(channel))
      : [
        new ChannelStore(new ChatChannel('general')),
        new ChannelStore(new ChatChannel('common')),
      ];

    this.channelStores.forEach((store) => {
      store.state$.subscribe((state) => {
        this.channelRepository.saveChannel(state);
      });
    });
  }

  public get channelNames(): string[] {
    return this.channelStores.map((store) => store.name);
  }

  public getLog(channelName: string): Observable<ChatLine[]> | null {
    const store = this.channelStores.find((channelStore) => channelStore.name === channelName);
    return store ? store.get('lines') : null;
  }

  public pushMessage(channelName: string, chatLine: ChatLine) {
    const store = this.channelStores.find((channelStore) => channelStore.name === channelName);
    if (!store) {
      return;
    }

    store.pushMessage(chatLine);
  }

  public createChannel(name: string) {
    const store = this.channelStores.find((channelStore) => channelStore.name === name);

    if (store) {
      throw new Error('channel already exists');
    }

    this.channelStores.push(new ChannelStore(new ChatChannel(name)));
  }
}

iocContainer.bind<ChatService>(ChatService).toSelf().inSingletonScope();
