import { inject, injectable } from 'inversify';
import { iocContainer } from '../utils/ioc-container';
import { UserRepository } from '../data/repositories/user.repository';
import User from '../models/user.model';

// TODO: This is just mock for Auth Service

@injectable()
export default class AuthService {
  constructor(
    @inject(UserRepository) private userRepository: UserRepository,
  ) { }

  async authenticate(credentials: { username: string, password: string }): Promise<User | undefined> {
    return this.userRepository.authenticate(credentials);
  }

  async isUserExist(username: string): Promise<boolean> {
    const user = await this.userRepository.getUserByUsername(username);

    return !!user;
  }

  async register(credentials: { username: string, password: string, name: string }): Promise<User | undefined> {
    return this.userRepository.register(credentials);
  }

  async getUserById(id: string): Promise<User | null> {
    return this.userRepository.getUser(id);
  }
}

iocContainer.bind<AuthService>(AuthService).toSelf().inSingletonScope();
