import { injectable } from 'inversify';
import { iocContainer } from '../../utils/ioc-container';
import ChatChannel from '../../../shared/models/chat-channel';
import { ChannelModel } from '../mongo-models/chat.mongo.model';

const instanceMapper = (chatChannelInstance: { [key: string]: any }) => new ChatChannel(
  chatChannelInstance.name,
  // eslint-disable-next-line no-underscore-dangle
  chatChannelInstance._id,
  chatChannelInstance.lines,
);

@injectable()
export class ChannelRepository {
  async getChat(id: string): Promise<ChatChannel | null> {
    const chatInstance = await ChannelModel.findById(id).lean();

    return chatInstance && instanceMapper(chatInstance);
  }

  async getChannelByName(name: string): Promise<ChatChannel | null> {
    const chatInstance = await ChannelModel.findOne({ name }).lean();

    return chatInstance && instanceMapper(chatInstance);
  }

  async getChannels(): Promise<ChatChannel[]> {
    const chatInstances = await ChannelModel.find().lean();
    return chatInstances.map(instanceMapper);
  }

  async saveChannel({ name, lines }: ChatChannel): Promise<void> {
    try {
      await ChannelModel.updateOne({ name }, { lines }, { upsert: true });
    } catch (e) {
      throw new Error('Unable to save channel');
    }
  }
}

iocContainer.bind<ChannelRepository>(ChannelRepository).toSelf().inSingletonScope();
