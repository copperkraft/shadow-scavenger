import { injectable } from 'inversify';
import { UserModel } from '../mongo-models/user.mongo.model';
import User from '../../models/user.model';
import { iocContainer } from '../../utils/ioc-container';
import { getHash } from '../../utils/hash';
import { getSalt } from '../../utils/salt';

const instanceMapper = (userInstances: { [key: string]: any }) => new User({
  // eslint-disable-next-line no-underscore-dangle
  id: userInstances._id,
  name: userInstances.name,
  username: userInstances.username,
});

@injectable()
export class UserRepository {
  async getUser(id: string): Promise<User | null> {
    const userInstance = await UserModel.findById(id);

    return userInstance && instanceMapper(userInstance);
  }

  async getUserByUsername(username: string): Promise<User | null> {
    const userInstance = await UserModel.findOne({ username });

    return userInstance && instanceMapper(userInstance);
  }

  async authenticate({ username, password }: { username: string, password: string }): Promise<User> {
    const userInstance = await UserModel.findOne({ username });

    if (!userInstance) {
      throw new Error('Unable to sign in');
    }

    if (userInstance.hash !== getHash(password, userInstance.salt)) {
      throw new Error('Unable to sign in');
    }

    return instanceMapper(userInstance);
  }

  async register({ username, password, name }: { username: string, password: string, name: string }): Promise<User> {
    if (await UserModel.findOne({ username })) {
      throw new Error('User already exists');
    }

    const salt = getSalt();
    const hash = getHash(password, salt);

    const userModel = new UserModel({
      username, name, hash, salt,
    });

    try {
      await userModel.save();
    } catch (e) {
      throw new Error('Unable to create user');
    }

    return instanceMapper(userModel);
  }
}

iocContainer.bind<UserRepository>(UserRepository).toSelf().inSingletonScope();

/* eslint-enable no-underscore-dangle, class-methods-use-this */
