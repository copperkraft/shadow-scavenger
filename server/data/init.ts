import { connect, set } from 'mongoose';
import { get } from 'config';

set('useCreateIndex', true);
set('useUnifiedTopology', true);

export const mongoInit = async () => {
  const uri = get<string>('mongoDbUrl');
  await connect(uri, { useNewUrlParser: true });
};
