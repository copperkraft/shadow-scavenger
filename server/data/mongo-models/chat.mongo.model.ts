// tslint:disable:variable-name
import { getModelForClass, prop } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import ChatLineModel from '../../models/chat-line.model';

class Channel {
  @prop()
  id!: Types.ObjectId;

  @prop({ required: true, unique: true })
  name!: string;

  @prop({ type: ChatLineModel })
  lines!: ChatLineModel[];
}

const ChannelModel = getModelForClass(Channel);

export { ChannelModel };
