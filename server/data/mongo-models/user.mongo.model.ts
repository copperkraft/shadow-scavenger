import { Types } from 'mongoose';
import { getModelForClass, prop } from '@typegoose/typegoose';

class User {
  @prop()
  id!: Types.ObjectId;

  @prop({ required: true, unique: true })
  username!: string;

  @prop({ required: true })
  name!: string;

  @prop({ required: true })
  hash!: string;

  @prop({ required: true })
  salt!: string;
}

const UserModel = getModelForClass(User);

export { UserModel };
