import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthHeaderComponent } from './layout/auth-header/auth-header.component';

@NgModule({
  declarations: [
    LayoutComponent,
    AuthHeaderComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
  ],
  exports: [
    LayoutComponent,
  ],
})
export class SharedModule {}
