import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  set<T = any>(key: string, value: T) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  get<T = any>(key: string): T | null {
    const rawValue = localStorage.getItem(key);
    if (!rawValue) {
      return null;
    }

    return JSON.parse(rawValue);
  }

  remove(key: string) {
    localStorage.removeItem(key);
  }
}
