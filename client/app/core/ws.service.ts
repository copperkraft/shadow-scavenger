import { Injectable } from '@angular/core';
import { combineLatest, Subject } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { AuthService } from './auth.service';
import { Message } from '../../../shared/models/message';
import { AuthMessage } from '../../../server/jobs/auth.job';

const wsProtocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
const wsConnectionString = `${wsProtocol}:${window.location.host}`;

@Injectable({
  providedIn: 'root',
})
export class WsService {
  private openSubject = new Subject<Event>();

  private wsSubject$: WebSocketSubject<any> = webSocket({
    url: wsConnectionString,
    openObserver: this.openSubject,
  });

  constructor(private authService: AuthService) {
    combineLatest(this.authService.currentToken$, this.openSubject)
      .subscribe(([token]) => this.auth(token || null));
  }

  public get ws$() {
    return this.wsSubject$.asObservable();
  }

  public send<T extends Message = Message>(message: T) {
    this.wsSubject$.next(message);
  }

  public auth(token: string | null) {
    this.send<AuthMessage>({
      type: 'auth',
      payload: token,
    });
  }
}
