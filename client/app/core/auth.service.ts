import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LocalStorageService } from './local-storage.service';
import User from '../../../server/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user$ = new BehaviorSubject<User | null>(this.localStorageService.get('currentUser'));

  private token$ = new BehaviorSubject<string | null>(this.localStorageService.get('token'));

  constructor(
    private http: HttpClient,
    private localStorageService: LocalStorageService,
  ) { }

  get currentUser$(): Observable<User | null> {
    return this.user$.asObservable();
  }

  get currentToken$(): Observable<string | null> {
    return this.token$.asObservable();
  }

  can(): Observable<boolean> {
    return this.currentUser$.pipe(
      map((user) => !!user),
    );
  }

  login(username: string, password: string): Observable<boolean> {
    return this.http.post<{ user: User, token: string }>('/api/auth/login', { username, password })
      .pipe(map(({ user, token }) => {
        this.setUser(user, token);
        return true;
      }));
  }

  register(username: string, password: string, name: string): Observable<boolean> {
    return this.http.post<any>('/api/auth/register', { username, password, name })
      .pipe(map(({ user, token }) => {
        this.setUser(user, token);
        return true;
      }));
  }

  logout() {
    this.localStorageService.remove('currentUser');
    this.localStorageService.remove('token');
    this.user$.next(null);
    this.token$.next(null);
  }

  private setUser(user: User, token: string) {
    this.localStorageService.set('currentUser', user);
    this.localStorageService.set('token', token);

    this.user$.next(user);
    this.token$.next(token);
  }
}
