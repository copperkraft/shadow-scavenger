import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { WsService } from './ws.service';
import { AuthService } from './auth.service';
import { LocalStorageService } from './local-storage.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    WsService,
    AuthService,
    LocalStorageService,
  ],
})
export class CoreModule {}
