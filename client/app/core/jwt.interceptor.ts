import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { first, mergeMap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.currentToken$.pipe(
      first(),
      mergeMap((token) => {
        if (!token) {
          return next.handle(request);
        }

        return next.handle(request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`,
          },
        }));
      }),
    );
  }
}
