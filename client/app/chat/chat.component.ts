import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, takeWhile } from 'rxjs/operators';
import { ChatService } from './chat.service';
import ChatLine from '../../../shared/models/chat-line';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {
  public inputMessage: string = '';

  public messages$?: Observable<ChatLine[]>;

  public channelName: string | null = null;

  constructor(public chatService: ChatService) { }

  ngOnInit(): void {
    this.chatService.getChannelList();
    this.chatService.channelNames$
      .pipe(
        takeWhile(() => !this.messages$),
        filter((channelNames) => !!channelNames.length),
      )
      .subscribe((channelNames) => {
        this.connectToChannel(channelNames[0]);
      });
  }

  connectToChannel(channelName: string): void {
    this.chatService.connect(channelName);
    this.messages$ = this.chatService.getMessages();
    this.channelName = this.chatService.getChannelName();
  }

  ngOnDestroy(): void {
    this.chatService.disconnect();
  }

  send(): void {
    if (!this.inputMessage) {
      return;
    }

    this.chatService.send({
      message: this.inputMessage,
    });

    this.inputMessage = '';
  }
}
