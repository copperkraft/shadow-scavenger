import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChatComponent } from './chat.component';
import { SharedModule } from '../shared/shared.module';
import { ChatService } from './chat.service';
import { ChatRoutingModule } from './chat-routing.module';
import { ChannelListComponent } from './channel-list/channel-list.component';

@NgModule({
  declarations: [
    ChatComponent,
    ChannelListComponent,
  ],
  providers: [
    ChatService,
  ],
  imports: [
    ChatRoutingModule,
    CommonModule,
    SharedModule,
    FormsModule,
  ],
})
export class ChatModule {}
