import {
  Component, EventEmitter, Input, Output,
} from '@angular/core';

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.scss'],
})
export class ChannelListComponent {
  @Input() public channels: string[] = [];

  @Input() public activeChannel: string | null = null;

  @Output() public onChannelChange = new EventEmitter<string>();
}
