import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import {
  BehaviorSubject, of, Subject, Subscription,
} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { WsService } from '../core/ws.service';
import ChatLine from '../../../shared/models/chat-line';
import {
  ChatMessage, ConnectToChatMessage, LogMessage, NewConnectionMessage,
} from '../../../server/jobs/chat.job';
import ChannelStore from '../../../shared/store/chat.store';
import ChatChannel from '../../../shared/models/chat-channel';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  public channelNames$ = new BehaviorSubject<string[]>([]);

  private channelStore: ChannelStore | undefined;

  private channelName: string | null = null;

  private leaveChat$ = new Subject<void>();

  private wsSubscription: Subscription | undefined;

  constructor(
    private wsService: WsService,
    private httpClient: HttpClient,
  ) { }

  public getChannelList(): void {
    this.httpClient.get<string[]>('/api/chats/channels')
      .subscribe((channelList) => {
        this.channelNames$.next(channelList);
      });
  }

  public getChannelName(): string | null {
    return this.channelName;
  }

  public getMessages(): Observable<ChatLine[]> {
    return this.channelStore ? this.channelStore.get('lines') : of([]);
  }

  public connect(channelName: string) {
    if (!this.channelName) {
      this.wsSubscription = this.wsService.ws$
        .pipe(takeUntil(this.leaveChat$))
        .subscribe((data) => this.messageReducer(data));
    }

    const channel = new ChatChannel(channelName);

    this.channelStore = new ChannelStore(channel);
    this.channelName = channelName;

    this.wsService.send<ConnectToChatMessage>({
      type: 'connect to channel',
      payload: channelName,
    });
  }

  public disconnect() {
    this.channelName = null;
    this.leaveChat$.next();
  }

  public send(message: ChatLine) {
    this.wsService.send<ChatMessage>({
      type: 'message',
      payload: {
        channel: this.channelName || 'general',
        line: message,
      },
    });
  }

  private messageReducer(data: ChatMessage | LogMessage | NewConnectionMessage) {
    const channel = data.payload && data.payload.channel;
    const store = this.channelStore;

    if (!channel || !store) {
      return;
    }

    switch (data.type) {
      case 'new connection':
        if (data.payload.channel !== this.channelName) {
          return;
        }
        store.pushMessage({
          sender: 'system',
          message: `${data.payload.name || 'Anonymous user'} connected to the "${data.payload.channel}"`,
          date: new Date(),
        });
        break;
      case 'log':
        store.setMessages(data.payload.log);
        break;
      case 'message':
        store.pushMessage(data.payload.line);
        break;
      default:
    }
  }
}
