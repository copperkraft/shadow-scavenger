import { Component, HostListener, OnInit } from '@angular/core';
import { add, dist } from 'vec-la';
import { mod } from '../../../../shared/utils/vector';

const directions: { [key: string]: Vector } = {
  left: [-1, 0],
  right: [1, 0],
  up: [0, -1],
  down: [0, 1],
};

@Component({
  selector: 'app-snake',
  templateUrl: './snake.component.html',
  styleUrls: ['./snake.component.scss'],
})
export class SnakeComponent implements OnInit {
  public snakeBody!: Vector[];

  public food!: Vector;

  public score: number = 0;

  public lights: {
    coordinates: Vector,
    color: string
  }[] = [];

  private settings = {
    initialSize: 4,
    field: [15, 15] as Vector,
    delay: 300,
    scale: 50,
    pointsPerFood: 10,
  };

  private direction!: Vector;

  private isDirectionChanged = false;

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.isDirectionChanged) return;
    const directionName = event.key.replace('Arrow', '').toLowerCase();

    const direction = directions[directionName];
    if (!direction) {
      return;
    }

    const [diffX, diffY] = add(direction, this.direction);
    if (!diffX && !diffY) {
      return;
    }

    this.direction = direction;

    this.isDirectionChanged = true;
  }

  public initialize() {
    this.direction = directions.right;
    this.score = 0;
    this.placeInitialBody();
    this.placeFood();
    this.nextStep();
  }

  ngOnInit() {
    this.initialize();
  }

  private placeInitialBody() {
    const [centerX, centerY] = this.settings.field.map((size) => Math.floor(size / 2));

    // tslint:disable-next-line:prefer-array-literal
    this.snakeBody = new Array((this.settings.initialSize))
      .fill('')
      .map<Vector>((item, index) => ([
      centerX - index,
      centerY,
    ]));
  }

  private placeFood() {
    do {
      this.food = this.settings.field.map((size) => Math.floor(size * Math.random())) as Vector;
    }
    while (this.snakeBody.some((body) => !dist(body, this.food)));
  }

  private nextStep() {
    this.isDirectionChanged = false;

    let nextHead = add(this.snakeBody[0], this.direction);

    nextHead = mod(add(nextHead, this.settings.field), this.settings.field);

    const foodWasEaten = !dist(this.food, nextHead);

    if (!foodWasEaten) {
      this.snakeBody.pop();
    }

    if (this.snakeBody.some((body) => !dist(body, nextHead))) {
      this.initialize();
      return;
    }

    this.snakeBody.unshift(nextHead);

    if (foodWasEaten) {
      this.score += this.settings.pointsPerFood;
      this.placeFood();
    }

    this.lights = [
      ...this.snakeBody.map((body) => ({ coordinates: body, color: 'yellow' })),
      { coordinates: this.food, color: 'red' },
    ];

    setTimeout(() => this.nextStep(), this.settings.delay);
  }
}
