import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiniGamesRoutingModule } from './mini-games-routing.module';
import { MiniGamesComponent } from './mini-games.component';
import { SnakeComponent } from './snake/snake.component';
import { SharedModule } from '../shared/shared.module';
import { LedScreenComponent } from './led-screen/led-screen.component';

@NgModule({
  declarations: [
    MiniGamesComponent,
    SnakeComponent,
    LedScreenComponent,
  ],
  imports: [
    CommonModule,
    MiniGamesRoutingModule,
    SharedModule,
  ],
})
export class MiniGamesModule {}
