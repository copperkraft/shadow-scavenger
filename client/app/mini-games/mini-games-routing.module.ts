import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SnakeComponent } from './snake/snake.component';

const routes: Routes = [
  {
    path: 'snake',
    component: SnakeComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'snake',
  },
  {
    path: '/*path',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MiniGamesRoutingModule {}
