import { Component, Input, OnChanges } from '@angular/core';

type Led = {
  color: string,
  on: boolean
};

@Component({
  selector: 'app-led-screen',
  templateUrl: './led-screen.component.html',
  styleUrls: ['./led-screen.component.scss'],
})
export class LedScreenComponent implements OnChanges {
  @Input()
  public width: number = 0;

  @Input()
  public height: number = 0;

  @Input()
  public lights: {
    coordinates: [number, number],
    color: string
  }[] = [];

  public field: Led[][] = [];

  ngOnChanges() {
    this.field = this.getEmptyField();

    this.lights.forEach((light) => {
      const [x, y] = light.coordinates;
      this.field[y][x] = {
        color: light.color,
        on: true,
      };
    });
  }

  trackLines(y: number) {
    return y;
  }

  trackItems(y: number) {
    return (x: number) => `${x}:${y}`;
  }

  private getEmptyField(): Led[][] {
    return [...Array(this.height)].map(() => [...Array(this.width)]);
  }
}
