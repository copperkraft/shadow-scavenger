import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule',
  },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
  },
  {
    path: 'chat',
    loadChildren: './chat/chat.module#ChatModule',
  },
  {
    path: 'mini-games',
    loadChildren: './mini-games/mini-games.module#MiniGamesModule',
  },
  {
    path: 'demos',
    loadChildren: './demos/demos.module#DemosModule',
  },
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full',
  },
];

export const routing = RouterModule.forRoot(appRoutes);
