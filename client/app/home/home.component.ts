import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  ledSwitchActive: boolean = true;

  ledChangeActive: boolean = true;

  glowButtonSwitchActive: boolean = true;

  glowButtonChangeActive: boolean = true;
}
