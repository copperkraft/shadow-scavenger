import { scale } from 'vec-la';
import { mapVector } from '../../../../shared/utils/vector';

type Map = number[][];

export class DiamondSquareGenerator {
  getMap(iterations: number, maxValue: number = 0, minValue: number = 100): Map {
    const map = this.getGrid(iterations);
    let max: number = map[0][0];
    let min: number = map[0][0];

    this.forEachMap(map, (value) => {
      max = max > value ? max : value;
      min = min < value ? min : value;
    });

    this.forEachMap(map, (value, [column, row]) => {
      // eslint-disable-next-line no-mixed-operators
      map[row][column] = Math.floor((value - min) / (max - min) * (maxValue - minValue) + minValue);
    });

    this.forEachMap(map, (value) => {
      max = max > value ? max : value;
      min = min < value ? min : value;
    });

    return map;
  }

  getGrid(iterations: number): Map {
    return iterations > 0
      ? this.doubleMap(this.getGrid(iterations - 1))
      : DiamondSquareGenerator.initMap();
  }

  private getEmptyMap([width, height]: Vector) {
    return [...Array(height)].map(() => [...Array(width)]);
  }

  private static initMap() {
    return [
      [1, 1],
      [1, 1],
    ];
  }

  private doubleMap(map: Map): Map {
    const size = [map[0].length || 0, map.length] as Vector;
    const newSize = mapVector(size, (value) => value * 2 - 1);

    const newMap = this.getEmptyMap(newSize);

    this.forEachMap(map, (value, [column, row]) => {
      newMap[row * 2][column * 2] = value * 3;
    });

    this.forEachMap(map, (value, place: Vector) => {
      const [column, row] = scale(place, 2);

      if (column >= newSize[0] - 1 || row >= newSize[1] - 1) {
        return;
      }

      newMap[row + 1][column + 1] = this.getAverage([
        newMap[row][column],
        newMap[row + 2] && newMap[row + 2][column],
        newMap[row][column + 2],
        newMap[row + 2] && newMap[row + 2][column + 2],
      ]) + Math.random() * 2 - 1;
    });

    this.forEachMap(map, (value, place: Vector) => {
      const [column, row] = scale(place, 2);

      if (column < newSize[0] - 1) {
        newMap[row][column + 1] = this.getAverage([
          newMap[row][column],
          newMap[row + 1] && newMap[row + 1][column + 1],
          newMap[row - 1] && newMap[row - 1][column + 1],
          newMap[row][column + 2],
        ]) + Math.random() * 2 - 1;
      }

      if (row < newSize[1] - 1) {
        newMap[row + 1][column] = this.getAverage([
          newMap[row][column],
          newMap[row + 1] && newMap[row + 1][column + 1],
          newMap[row + 1] && newMap[row + 1][column - 1],
          newMap[row + 2][column],
        ]) + Math.random() * 2 - 1;
      }
    });

    this.forEachMap(newMap, (value, [column, row]) => {
      newMap[row][column] = value || 0;
    });

    return newMap;
  }

  private getAverage(values: (number | undefined)[]): number {
    const actualValues = values.filter((value) => value !== undefined) as number[];

    return actualValues.reduce((sum, value) => sum + value) / actualValues.length;
  }

  private forEachMap(map: Map, cb: (value: number, point: Vector) => void) {
    map.forEach((line, row) => {
      line.forEach((value, column) => {
        cb(value, [column, row]);
      });
    });
  }
}
