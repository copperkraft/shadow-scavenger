import { Component, OnInit } from '@angular/core';
import { DiamondSquareGenerator } from './diamond-square-generator';

@Component({
  selector: 'app-diamond-square',
  templateUrl: './diamond-square.component.html',
  styleUrls: ['./diamond-square.component.scss'],
})
export class DiamondSquareComponent implements OnInit {
  map: number[][] = [];

  generator: DiamondSquareGenerator = new DiamondSquareGenerator();

  iterations = 4;

  minValue = 10; // min and max values picked so, that we can use them as opacity value

  maxValue = 99;

  ngOnInit() {
    this.map = this.generator.getMap(this.iterations, this.minValue, this.maxValue);
  }

  rerollGrid() {
    this.map = this.generator.getMap(this.iterations, this.minValue, this.maxValue);
  }
}
