import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemosRoutingModule } from './demos-routing.module';
import { DemosComponent } from './demos.component';
import { FractalComponent } from './fractal/fractal.component';
import { SharedModule } from '../shared/shared.module';
import { DiamondSquareComponent } from './diamond-square/diamond-square.component';
import { HexMapComponent } from './hex-map/hex-map.component';

@NgModule({
  declarations: [
    DemosComponent,
    FractalComponent,
    HexMapComponent,
    DiamondSquareComponent,
  ],
  imports: [
    CommonModule,
    DemosRoutingModule,
    SharedModule,
  ],
})
export class DemosModule { }

/* CommonModule,
    MiniGamesRoutingModule,
    CommonModule,
    SharedModule */
