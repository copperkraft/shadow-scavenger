export const textureUrls = [
  'https://media.freestocktextures.com/cache/c4/c6/c4c662e131f1603284329fa1ec59eb29.jpg', // wotah
  'https://media.freestocktextures.com/cache/54/21/542127aa2861d193726e67541dc171be.jpg', // sand
  'https://media.freestocktextures.com/cache/ce/b9/ceb9aa08da9a69fcaeeae13885481553.jpg', // gras
  'https://media.freestocktextures.com/cache/52/ce/52ceff0b3aec3069c247946b3bbebb63.jpg', // fild
  'https://media.freestocktextures.com/cache/04/07/040748f5897a0ad446cb6e48d816fbfe.jpg', // forest
];
