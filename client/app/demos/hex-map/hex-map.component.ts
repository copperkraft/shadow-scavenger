import {
  Component, ElementRef, OnInit, ViewChild,
} from '@angular/core';
import { add, scale } from 'vec-la';
import { Observable } from 'rxjs';
import { Hex, HexMap } from '../../../../shared/hex';
import { applyVector } from '../../../../shared/utils/vector';
import { getHexHeightMap } from '../../../../shared/game/mapgen/fractal-height';

@Component({
  selector: 'app-hex-map',
  templateUrl: './hex-map.component.html',
  styleUrls: ['./hex-map.component.scss'],
})
export class HexMapComponent implements OnInit {
  @ViewChild('workbench', { static: true })
  private htmlWorkbenchCanvas!: ElementRef<HTMLCanvasElement>;

  private workbenchContext!: CanvasRenderingContext2D;

  private fieldSize: Vector = [900, 600];

  private setUp() {
    [
      this.htmlWorkbenchCanvas.nativeElement.width,
      this.htmlWorkbenchCanvas.nativeElement.height,
    ] = this.fieldSize;

    const context = this.htmlWorkbenchCanvas.nativeElement.getContext('2d');

    if (!context) {
      throw new Error('Context undefined');
    }

    this.workbenchContext = context;
    this.workbenchContext.fillStyle = 'white';
    this.workbenchContext.fillRect(0, 0, this.fieldSize[0], this.fieldSize[1]);
    this.workbenchContext.strokeStyle = 'rgba(0, 0, 0, 0.5)';
    this.workbenchContext.fillStyle = 'red';
  }

  ngOnInit() {
    this.setUp();
    const map = getHexHeightMap(20);
    this.drawMap(map);
  }

  drawMap(map: HexMap) {
    map.forEachCell((cell) => this.drawCell(cell));
  }

  drawCell(cell: Hex) {
    const size = 40;
    const hexagon = cell.vertices.map((point) => add(
      applyVector(point, [size, size * 0.6], (a, b) => a * b),
      scale(this.fieldSize, 0.5),
    ));

    this.drawHexagon(hexagon, cell.data);
  }

  textureElements: Promise<HTMLImageElement>[] = [
    'https://media.freestocktextures.com/cache/c4/c6/c4c662e131f1603284329fa1ec59eb29.jpg', // wotah
    'https://media.freestocktextures.com/cache/54/21/542127aa2861d193726e67541dc171be.jpg', // sand
    'https://media.freestocktextures.com/cache/ce/b9/ceb9aa08da9a69fcaeeae13885481553.jpg', // gras
    'https://media.freestocktextures.com/cache/52/ce/52ceff0b3aec3069c247946b3bbebb63.jpg', // fild
    'https://media.freestocktextures.com/cache/04/07/040748f5897a0ad446cb6e48d816fbfe.jpg', // forest
  ].map((url) => new Observable<HTMLImageElement>((observer) => {
    const img = new Image();
    img.src = url;
    img.onload = () => {
      observer.next(img);
      observer.complete();
    };
  }).toPromise());

  getRandomTexture(data: number) {
    const index = Math.floor((data + 1) * this.textureElements.length * 0.5);
    return this.textureElements[index];
  }

  async drawHexagon(hexagon: Vector[], data: number) {
    const ctx = this.workbenchContext;

    const img = await this.getRandomTexture(data);
    const pattern = ctx.createPattern(img, 'repeat');
    if (!pattern) {
      return;
    }
    pattern.setTransform({
      a: 0.2 + Math.random() * 0.1,
      d: 0.2 + Math.random() * 0.1,
      e: hexagon[1][0] - Math.random() * 50,
      f: hexagon[3][1] - Math.random() * 50,
    });
    ctx.fillStyle = pattern;
    ctx.beginPath();

    ctx.moveTo(...hexagon[hexagon.length - 1]);
    hexagon.forEach((point) => this.workbenchContext.lineTo(...point));

    ctx.fill();
    ctx.closePath();
  }
}
