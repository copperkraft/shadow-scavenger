import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemosComponent } from './demos.component';
import { FractalComponent } from './fractal/fractal.component';
import { DiamondSquareComponent } from './diamond-square/diamond-square.component';
import { HexMapComponent } from './hex-map/hex-map.component';

const routes: Routes = [
  {
    path: 'fractal',
    component: FractalComponent,
  },
  {
    path: 'hex-map',
    component: HexMapComponent,
  },
  {
    path: 'diamond-square',
    component: DiamondSquareComponent,
  },
  {
    path: '',
    component: DemosComponent,
  },
  {
    path: '/*path',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DemosRoutingModule { }
