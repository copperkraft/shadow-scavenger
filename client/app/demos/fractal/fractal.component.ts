import {
  AfterViewInit, Component, ElementRef, ViewChild,
} from '@angular/core';
import {
  dist, norm, rotatePointAround, sub, towards,
} from 'vec-la';

@Component({
  selector: 'app-fractal',
  templateUrl: './fractal.component.html',
  styleUrls: ['./fractal.component.scss'],
})
export class FractalComponent implements AfterViewInit {
  private width = 900;

  private height = 600;

  @ViewChild('workbench')
  private workbenchCanvas!: ElementRef<HTMLCanvasElement>;

  private workbenchContext: CanvasRenderingContext2D | null = null;

  private workPoints: Vector[] = [];

  private setUp() {
    this.workbenchCanvas.nativeElement.width = this.width;
    this.workbenchCanvas.nativeElement.height = this.height;
    this.workbenchContext = this.workbenchCanvas.nativeElement.getContext('2d');
    if (!this.workbenchContext) {
      return;
    }
    this.workbenchContext.fillStyle = 'white';
    this.workbenchContext.fillRect(0, 0, this.width, this.height);
    this.workbenchContext.strokeStyle = 'rgba(0, 0, 0, 0.5)';
    this.workbenchContext.fillStyle = 'red';
  }

  private resetPoints() {
    this.workPoints = [
      [this.width * 0.2, this.height * 0.7],
      [this.width * 0.4, this.height * 0.7],
      [this.width * 0.5, this.height * 0.7 - this.width * 0.1 * 3 ** 0.5],
      [this.width * 0.6, this.height * 0.7],
      [this.width * 0.8, this.height * 0.7],
    ];
  }

  private get startPoint() {
    return this.workPoints[0];
  }

  private get endPoint() {
    return this.workPoints[this.workPoints.length - 1];
  }

  private computeSequence(begin: Vector, end: Vector) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [startPoint, ...restPoints] = this.workPoints;

    return restPoints.map((point) => {
      const relativeDistance = dist(point, this.startPoint) / dist(this.endPoint, this.startPoint);
      const relativeAngle = Math.acos(norm(sub(point, this.startPoint))[0]);
      const pointOnSegment = towards(begin, end, relativeDistance);
      return rotatePointAround(pointOnSegment, begin, -relativeAngle);
    });
  }

  private complicate(points: Vector[], power: number = 1): Vector[] {
    const [startPoint, ...restPoints] = points;

    const complicationResult = [startPoint, ...restPoints.reduce(
      (newSequence: Vector[], point: Vector, index) => [
        ...newSequence,
        ...this.computeSequence(points[index], point),
      ],
      [],
    )];

    return power > 1 ? this.complicate(complicationResult, power - 1) : complicationResult;
  }

  private redrawCanvas(points: Vector[]) {
    if (!this.workbenchContext) {
      return;
    }

    this.workbenchContext.beginPath();
    this.workbenchContext.moveTo(...this.startPoint);

    points.forEach((point: Vector) => {
      if (!this.workbenchContext) {
        return;
      }

      this.workbenchContext.lineTo(...point);
    });

    this.workbenchContext.stroke();
  }

  ngAfterViewInit() {
    this.setUp();
    this.resetPoints();
    this.redrawCanvas(
      this.complicate(this.workPoints, 4),
    );
  }
}
