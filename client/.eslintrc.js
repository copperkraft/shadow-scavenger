module.exports = {
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: "module",
        project: "./client/tsconfig.app.json",
    },
    extends: [
        "airbnb-typescript/base"
    ],
    rules: {
        "import/prefer-default-export": 0,
        "max-len": ["error", { "code": 120 }],
        "linebreak-style": 0,
        "class-methods-use-this": 0,
    }
};
