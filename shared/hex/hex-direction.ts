export enum HexDirection {
  Left = 0, // [1, 0]
  TopLeft = 1, // [1, -1]
  TopRight = 2, // [0, -1]
  Right = 3, // [-1, 0]
  BottomRight = 4, // [-1, 1]
  BottomLeft = 5 // [0, 1]
}

export const directionVectors: Vector[] = [
  [1, 0],
  [1, -1],
  [0, -1],
  [-1, 0],
  [-1, 1],
  [0, 1]
];
