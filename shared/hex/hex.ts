import { directionVectors, HexDirection } from './hex-direction';
import { add, rotate } from 'vec-la';

export class Hex<T = any> {
  constructor(
    public q: number,
    public r: number,
    public data?: T
  ) { }

  /*
        \
     ____\____Q
          \
           \R
     */

  get s(): number { // for using cubic coordinates
    return 0 - this.q - this.r;
  }

  get coordinates(): Vector {
    return [this.q, this.r];
  }

  get position(): Vector {
    return [
      this.q + this.r * 0.5,
      this.r * (3 ** 0.5) / 2
    ];
  }

  get vertices(): Vector[] {
    return [...Array(6)].map((item, index) => {
      const shift = rotate([0, 1 / 3 ** 0.5], index * Math.PI / 3);
      return add(this.position, shift);
    });
  }

  // get neighbours(): Vector[]

  public getNeighbourCoordinate(direction: HexDirection) {
    return add(this.coordinates, directionVectors[direction]);
  }

  // static fromPosition([x, y]: Vector) {
  //   return new Hex([
  //
  //   ]);
  // }
}
