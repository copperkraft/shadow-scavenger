import { Vector } from '../utils/vector';
import { Hex } from './hex';

export class HexMap<T = any> {
  private map: Hex<T>[][] = [];
  private cells: Hex<T>[] = [];

  constructor() {

  }

  public get([q, r]: Vector): Hex<T> | undefined {
    return this.map[q] && this.map[q][r];
  }

  public set([q, r]: Vector, cell: Hex<T>) {
    this.map[q] = this.map[q] || [];
    if (!this.map[q][r]) {
      this.cells.push(cell);
    }
    this.map[q][r] = cell;
  }

  public getByPosition([x, y]: Vector): Hex<T> | undefined {
    let r = y / (3 ** 0.5) * 2;
    let q = x - r * 0.5;

    const fractional = [q, r, 0 - q - r];
    const rounded = fractional.map(value => Math.round(value));
    const diff = rounded.map((value, index) => value - fractional[index]);

    if (diff[0] > diff[1] && diff[0] > diff[2]) {
      q = -r - diff[2];
    } else if (diff[1] > diff[2]) {
      r = -q - diff[2];
    }

    return this.get([q, r]);
  }

  public forEachCell(cb: (cell: Hex<T>) => void) {
    this.cells.forEach(cb);
  }

  static getEmptyMap<T>(radius: number): HexMap<T> {
    const map = new HexMap();
    for (let r = -radius; r <= radius; r += 1) {
      for (let q = -radius; q <= radius; q += 1) {
        if (q + r <= radius && q + r >= -radius) {
          map.set([q, r], new Hex<T>(q, r));
        }
      }
    }

    return map;
  }
}
