export type Vector = [number, number];
import { sub } from 'vec-la';

export const applyVector = (v1: Vector, v2: Vector, cb: (a: number, b: number) => number) => {
  return v1.map((value, dimension) => cb(value, v2[dimension])) as Vector;
};

export const mapVector = (v: Vector, cb: (a: number, dimension: number, v: Vector) => number) => {
  return v.map(
    (value, dimension, vector) => cb(value, dimension, vector as Vector)
  ) as Vector;
};

export const isSameVector = ([ax, ay]: Vector, [bx, by]: Vector) => {
  return ax === bx && ay === by;
};

export const changeBasis = (v: Vector, start: Vector, scales: Vector) => {
  return sub(v, start);
};

export const mod = (v1: Vector, v2: Vector) => {
  return applyVector(v1, v2, (a, b) => a % b);
};
