export default interface ChatLine {
  date?: Date;
  sender?: string;
  message: string;
}
