export interface Message<Type extends string = string, PayloadType = any> {
  type: Type;
  payload: PayloadType;
}
