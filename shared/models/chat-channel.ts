import ChatLine from './chat-line';

export default class ChatChannel {
  constructor(
    public name: string,
    public id?: string,
    public lines: ChatLine[] = [],
  ) { }
}
