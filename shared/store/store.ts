import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';
import { map, first } from 'rxjs/operators';

export class Store<T extends object> {
  protected stateSubject$: BehaviorSubject<T>;

  constructor(initialState: T) {
    this.stateSubject$ = new BehaviorSubject(initialState);
  }

  public get state$(): Observable<T> {
    return this.stateSubject$.asObservable();
  }

  public set(nextState: T): void {
    this.stateSubject$.next(nextState);
  }

  public path(nextState: Partial<T>): void {
    this.update(state => ({ ...state, ...nextState }));
  }

  public update(getNextState: (state: T) => T) : void {
    this.stateSubject$.pipe(first()).subscribe(
      state => this.stateSubject$.next(getNextState(state))
    );
  }

  public get<K extends keyof T>(key: K): Observable<T[K]> {
    return this.stateSubject$.pipe(
      map((state: T) => state[key])
    );
  }
}
