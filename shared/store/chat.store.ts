import ChatLine from '../models/chat-line';
import { Store } from './store';
import ChatChannel from '../models/chat-channel';

export default class ChannelStore extends Store<ChatChannel> {
  public name: string;

  constructor(channel: ChatChannel) {
    super(channel);
    this.name = channel.name;
  }

  pushMessage(newMessage: ChatLine) {
    this.update(state => ({
      ...state,
      lines: [...state.lines, newMessage]
    }));
  }

  setMessages(messages: ChatLine[]) {
    this.path({
      lines: messages
    });
  }
}
