export default interface ControlAgent {
  makeDecision: (timeAmount: number) => Promise<any>;
}
