import ControlFlowParameters from './control-flow-parameters.interface';

export const defaultControlFlowParameters: ControlFlowParameters = {
  timeUnits: [
    {
      name: 'minute',
      quantity: 1,
    },
    {
      name: 'hour',
      quantity: 60,
    },
    {
      name: 'day',
      quantity: 1440,
    },
    {
      name: 'week',
      quantity: 10080,
    },
    {
      name: 'month',
      quantity: 40320,
    },
  ],
  turnDuration: 60,
};
