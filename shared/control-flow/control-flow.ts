import ControlFlowParameters from './control-flow-parameters.interface';
import { defaultControlFlowParameters } from './default-control-flow-parameters';
import ControlAgent from './control-agent.interface';

export default class ControlFlow {
  public timeLine: any[] = [];
  public turnTime: number = 0;

  public agentSlots: {
    agent: ControlAgent,
    timeShift: number,
  }[] = [];

  constructor(private config: ControlFlowParameters = defaultControlFlowParameters) {

  }

  public addAgent(agent: ControlAgent) {
    this.agentSlots.push({
      agent,
      timeShift: this.turnTime,
    });

    this.agentSlots.sort((left, right) => left.timeShift - right.timeShift);
  }

  public async nextTurn() {
    this.turnTime += 1;
    const currentSlots = this.agentSlots.filter(slot => slot.timeShift === this.turnTime);

    for (const slot in currentSlots) {
      await currentSlots[slot].agent.makeDecision(this.config.turnDuration);
    }
  }
}
