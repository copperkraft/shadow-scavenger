export default interface ControlFlowParameters {
  timeUnits: { name: string, quantity: number }[];
  turnDuration: number;
}
