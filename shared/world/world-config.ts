export interface WorldConfig {
  name: string;
}

export const defaultWorldConfig: WorldConfig = {
  name: 'World'
};
