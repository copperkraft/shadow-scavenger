import { defaultWorldConfig, WorldConfig } from './world-config';
import { Region } from './region';
import { World } from './world';

export function generateWorld(config: WorldConfig = defaultWorldConfig): World {
  const map: Region[] = [];

  const world = new World(config);

  return world;
}
