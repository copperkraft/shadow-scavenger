import { HexMap } from '../../hex';
import SimplexNoise from 'simplex-noise';
import { scale } from 'vec-la';

export const getHexHeightMap = (size: number) => {
  const map = HexMap.getEmptyMap<number>(size);

  const generator = new SimplexNoise('str');

  map.forEachCell(((cell) => {
    const position = scale(cell.position, 0.08);
    return cell.data = generator.noise2D(...position);
  }));

  return map;
};
