import { Hex } from '../../hex';

export interface Lake {
  cells: Hex<Landscape>[];
}

export class Landscape {
  drainTo?: Hex<Landscape>;
  elevation: number = 0;
  lake?: Lake;
  type: string = 'unknown';
  drainFor: Hex<Landscape>[] = [];
  waterLevel: number = 0;
}
