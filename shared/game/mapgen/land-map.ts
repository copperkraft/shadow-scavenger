import { HexMap } from '../../hex';
import { Lake, Landscape } from './landscape';

export class LandMap extends HexMap<Landscape> {
  lakes: Lake[] = [];

  constructor() {
    super();

    this.forEachCell(cell => cell.data = new Landscape());
  }
}
