import { HexMap } from '../../hex';

interface MapgenConfig {
  size: number;
  evenness: number;
  woodiness: number;
}

const defaultMapgenConfig: MapgenConfig = {
  size: 20,
  evenness: 1,
  woodiness: 0.5
};

export const generateMap = (config: MapgenConfig, random = Math.random()) => {
  const map = HexMap.getEmptyMap(config.size);

  return map;
};
